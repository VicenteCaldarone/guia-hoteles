$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $(".carousel").carousel({
      interval: 2000
    });
    //-- Eventos sobre el Modal de Contacto
    $('#contacto').on('show.bs.modal', function(e){
      //-- cambio cambio estilo del boton y lo deshabilito
      $('#contactoBtn0701').removeClass('btn-outline-success');
      $('#contactoBtn0701').addClass('btn-light');
      $('#contactoBtn0701').prop('disabled', true);

      console.log('El modal se está mostrando');
    })
    $('#contacto').on('shown.bs.modal', function(e){
      console.log('El modal se mostró');
    })
    $('#contacto').on('hide.bs.modal', function(e){
      //-- vuelvo el boton al estilo original y lo habilito
      $('#contactoBtn0701').removeClass('btn-light');
      $('#contactoBtn0701').addClass('btn-outline-success');
      $('#contactoBtn0701').prop('disabled', false);

      console.log('El modal se está ocultando');
    })
    $('#contacto').on('hidden.bs.modal', function(e){
      console.log('El modal se ocultó');
    })
  });